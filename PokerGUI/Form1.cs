﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokerGUI
{
    public partial class Form1 : Form
    {

        List<PictureBox> pbList = new List<PictureBox>();

        public Form1()
        {
            InitializeComponent();
            pbList.Add(Card1);
            pbList.Add(Card2);
            pbList.Add(Card3);
            pbList.Add(Card4);
            pbList.Add(Card5);
        }


        private void Card1_Click_1(object sender, EventArgs e)
        {

            if (sender == Card1 && Card1.Top == 415)
                Card1.Top = Card1.Top - 15;
            else if (sender == Card2 && Card2.Top == 415)
                Card2.Top = Card2.Top - 15;
            else if (sender == Card3 && Card3.Top == 415)
                Card3.Top = Card3.Top - 15;
            else if (sender == Card4 && Card4.Top == 415)
                Card4.Top = Card4.Top - 15;
            else if (sender == Card5 && Card5.Top == 415)
                Card5.Top = Card5.Top - 15;

            foreach(PictureBox card in pbList)
            {
                if (card.Top == 400)
                {
                    Bttn1.Text = "Draw";
                }
            }         
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bttn1.Text = "Hold";
            foreach (PictureBox card in pbList)
            {
                if (card.Top == 400)
                    card.Top = card.Top + 15;
            }
        }

        public List<int> GetCardNum()
        {
            List<int> levantas = new List<int>();

            for(int i = 0; i < pbList.Count ; i++)
            {
                if (pbList[i].Top == 400)
                {
                    levantas.Add(i);
                }
            }

            return levantas;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<int> lista = new List<int>();
            lista = GetCardNum();
            foreach(int pos in lista)
            {
                MessageBox.Show(pos.ToString());
            }
        }
    }
}
