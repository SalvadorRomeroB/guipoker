﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePoker.Enums
{
    public enum Palo {Corazon, Espada, Diamante, Trebol}

    class EPalo
    {
        public static Palo getPalo(int num)
        {
            switch (num)
            {
                case 0:
                    return Palo.Espada;
                case 1:
                    return Palo.Corazon;
                case 2:
                    return Palo.Diamante;
                case 3:
                    return Palo.Trebol;
                default:
                    return 0;
            }
        }
    }
}
